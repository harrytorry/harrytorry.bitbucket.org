function areweonline() {
    var state = document.getElementById('state');
    state.innerHTML = (navigator.onLine) ? 'online' : 'offline';

    window.addEventListener("offline", function(e) {
        alert("offline");
    }, false);

    window.addEventListener("online", function(e) {
        alert("online");
    }, false);
}

var count = 0;
function addCount() {
    count++;
    var clickCounter = document.getElementById('clickCount');
    clickCounter.innerHTML = count;
}


/*
    Possible to do this with events, but there are some browsers that don't support it

    Just trying out the upload every 5 seconds
 */
function uploadData() {
    setTimeout(function () {
        if (navigator.onLine && count > 0) {
            count = 0;
            var clickCounter = document.getElementById('clickCount');
            clickCounter.innerHTML = 'uploaded! (so theres now 0 clicks)';
        }
        uploadData();
    }, 5000);
}



